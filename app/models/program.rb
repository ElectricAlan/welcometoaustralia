class Program < ActiveRecord::Base
  has_many  :volunteers, :class_name => 'User', through: :programs_users
  has_many :programs_users
  accepts_nested_attributes_for :volunteers
end
