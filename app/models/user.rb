class User < ActiveRecord::Base
  has_many :programs, :class_name => 'Program', through: :programs_users
  has_many :programs_users
  accepts_nested_attributes_for :programs
end
