class LocationsController < ApplicationController
  def index
    @locations = Location.all
  end

  def new
    @location = Location.new
  end

  def edit
    @location = Location.find(params[:id])
    @managers_array = User.where(is_admin: true).pluck(:name, :id)
  end

  def update
    @location = Location.find(params[:id])

    if @location.update(location_params)
      flash[:notice] = "Location Sucessfully Updated"
    else
      flash[:notice] = "Error Updating Location"
    end

    redirect_to action: 'edit'
  end

  def create
    @location = Location.new(location_params)

    if @location.save
      redirect_to @location
    else
      render 'new'
    end
  end

  def show
    @location = Location.find(params[:id])
    @managers_array = User.where(is_admin: true).pluck(:name, :id)
  end

  def destroy
    @location = Location.find(params[:id])
    @location.destroy

    redirect_to locations_path
  end

  private
    def location_params
      params.require(:location).permit(:suburb, :state, :address, :manager, :phone)
    end

end
