class UsersController < ApplicationController

  require 'pry'

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def update

    @user = User.find(params[:id])

    binding.pry

    #params[:programs_attributes].each do |p|

    #end



    if @user.update(user_params)
      flash[:notice] = "User Sucessfully Updated"
    else
      flash[:notice] = "Error Updating User"
    end

    redirect_to action: 'edit'
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to users_path
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :phone, :address, :is_admin, :is_volunteer, programs_attributes: [:id, :_destroy])
    end
end