class ProgramsController < ApplicationController

  def index
    @programs = Program.all
  end

  def new
    @program = Program.new
  end

  def edit
    @program = Program.find(params[:id])
  end

  def update
    @program = Program.find(params[:id])

    if @program.update(program_params)
      flash[:notice] = "Program Sucessfully Updated"
    else
      flash[:notice] = "Error Updating Program"
    end

    redirect_to action: 'edit'
  end

  def create
    @program = Program.new(program_params)

    if @program.save
      redirect_to @program
    else
      render 'new'
    end
  end

  def show
    @program = Program.find(params[:id])
  end

  def destroy
    @program = Program.find(params[:id])
    @program.destroy

    redirect_to programs_path
  end

  private
    def program_params
      params.require(:program).permit(:name, :coordinator, :description, :active)
    end

end