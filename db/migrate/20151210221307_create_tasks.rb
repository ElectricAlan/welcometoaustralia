class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.integer :location
      t.date :date
      t.integer :estimated_hours
      t.integer :coordinator

      t.timestamps
    end
  end
end
