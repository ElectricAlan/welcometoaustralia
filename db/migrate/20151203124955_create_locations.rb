class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :state
      t.string :phone
      t.text :address
      t.integer :manager

      t.timestamps
    end
  end
end
