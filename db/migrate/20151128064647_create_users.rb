class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.text :address
      t.boolean :is_admin
      t.boolean :is_volunteer
      t.date :induction_date
      t.integer :induction_location

      t.timestamps
    end
  end
end
