Rails.application.routes.draw do

  resources :users
  resources :tasks
  resources :skills
  resources :locations
  resources :tasks
  resources :events
  resources :programs

  get 'welcome/index'

  root 'welcome#index'
end
